//
//  SettingsService.swift
//  Laws
//
//  Created by User on 1/29/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

class SettingsService{
    private var lang: Lang
    
    init() {
        lang = Lang.ru
        if let langText = getFromFile(), let lang = Lang(rawValue: langText){
            self.lang = lang
        }else{
            setLang(lang: Lang.ru)
        }
        
        Local = "\(lang.rawValue)"
        UserDefaults.standard.set([lang.rawValue], forKey: "AppleLanguages")
//        UserDefaults.standard.synchronize()
    }
    
    func getCurrentLang() -> Lang{
        return lang
    }
    
    func setLang(lang: Lang){
        self.lang = lang
        self.saveToFile()
        
        Local = "\(lang.rawValue)"
        UserDefaults.standard.set([lang.rawValue], forKey: "AppleLanguages")
        
        
//        let path = Bundle.main.path(forResource: self.lang.rawValue, ofType: "lproj")
//        let bundle = Bundle(path: path!)
//        let string = bundle?.localizedString(forKey: "key", value: nil, table: nil)
//        UserDefaults.standard.synchronize()
    }
    
    private func getDocumentPath() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0].appendingPathComponent("settings.txt")
        return documentsDirectory
    }
    
    private func saveToFile(){
        do {
            let langText = lang.rawValue
            let fileURL = self.getDocumentPath()
            try langText.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        }
        catch {
            print(error)
        }
    }
    
    private func getFromFile() -> String!{
        do{
            let string = try String.init(contentsOf: self.getDocumentPath())
            return string
        }
        catch{
            print("Error: while trying to convert JSON to data")
            print(error)
        }
        return nil
    }

}
