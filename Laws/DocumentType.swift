//
//  DocumentType.swift
//  Laws
//
//  Created by User on 11/19/17.
//  Copyright © 2017 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct DocumentType: Codable {
    private(set) var Code: String?
    private (set) var Name: String
    var Types: [DocumentType]?
    
    func transform() -> Item {
        var item = Item(code: self.Code!, name: self.Name, list: nil)
        if self.Types != nil{
            for classificator in self.Types!{
                item.appendItem(item: classificator.transform())
            }
        }
        return item
    }
}

class DocumentTypeRepository: Repository<DocumentType>{
    init() {
        super.init(endpoint: "\(Host)/OpenData/GetClassificators.json?&Type=Type")
    }
}
