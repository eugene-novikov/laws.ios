//
//  UnitOfWork.swift
//  Laws
//
//  Created by User on 1/19/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

class UnitOfWork{
    private static let sharedUnitOfWork: UnitOfWork = {
        let uow = UnitOfWork()
        return uow
    }()
    
    private init(){
        authorityRep = AuthorityRepository()
        classRep = ClassRepository()
        documentTypeRep = DocumentTypeRepository()
        sourcePublicationRep = SourcePublicationRepository()
        keywordRep = KeywordRepository()
        generalClassifierRep = GeneralClassifierRepository()
        statusRepository = StatusRepository()
        
        favoriteService = FavoriteService()
        settingsService = SettingsService()
        historyService = HistoryService()
    }
    
    class func resetCache() {
        shared().authorityRep.resetCache()
        shared().classRep.resetCache()
        shared().documentTypeRep.resetCache()
        shared().sourcePublicationRep.resetCache()
        shared().keywordRep.resetCache()
        shared().generalClassifierRep.resetCache()
        shared().statusRepository.resetCache()
    }
        
    class func shared() -> UnitOfWork {
        return sharedUnitOfWork
    }
    
    let authorityRep: AuthorityRepository
    let classRep: ClassRepository
    let documentTypeRep: DocumentTypeRepository
    let sourcePublicationRep: SourcePublicationRepository
    let keywordRep: KeywordRepository
    let generalClassifierRep: GeneralClassifierRepository
    let statusRepository: StatusRepository
    
    let favoriteService: FavoriteService
    let settingsService: SettingsService
    let historyService: HistoryService
}
