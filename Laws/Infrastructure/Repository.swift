//
//  Repository.swift
//  Laws
//
//  Created by User on 11/19/17.
//  Copyright © 2017 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

class Repository<T> where T:Codable {
    
    private let endpoint: String
    private var elements: [T]?

    init(endpoint: String){
        self.endpoint = endpoint
    }
    
    func get(completionHandler: @escaping(T?, Error?)->Void){
        if !ReachabilityTest.isConnectedToNetwork(){
            let error = BackendError.noInternetConnection
            completionHandler(nil, error)
            return
        }
        guard let url = URL(string: self.endpoint)else{
            print("Error: Cannot create URL")
            let error = BackendError.urlError
            completionHandler(nil,error)
            return
        }
        var request = URLRequest(url: url)
        request.setValue(Local, forHTTPHeaderField: "Accept-Language")
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,error) in
            guard let responseData = data else{
                print("Error: did not receive data")
                completionHandler(nil,error)
                return
            }
            
            guard error==nil else {
                completionHandler(nil,error)
                return
            }
            
            let decoder = JSONDecoder()
            do{
                let entities = try decoder.decode(T.self,from: responseData)
                completionHandler(entities,nil)
            }
            catch{
                print("Error: while trying to convert JSON to data")
                print(error)
                print(responseData)
                completionHandler(nil,error)
            }
        }
        task.resume()
    }
    
    func getAll(completionHandler: @escaping([T]?, Error?)->Void){
        if !ReachabilityTest.isConnectedToNetwork(){
            let error = BackendError.noInternetConnection
            completionHandler(nil, error)
        }
        if self.elements?.count ?? 0 > 0 {
            completionHandler(elements, nil)
            return
        }
        
        print("\(self.endpoint)")
        guard let url = URL(string: self.endpoint)else{
            print("Error: Cannot create URL")
            let error = BackendError.urlError
            completionHandler(nil,error)
            return
        }
        var request = URLRequest(url: url)
        request.setValue(Local, forHTTPHeaderField: "Accept-Language")
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,error) in
            guard let responseData = data else{
                print("Error: did not receive data")
                completionHandler(nil,error)
                return
            }
            
            guard error==nil else {
                completionHandler(nil,error)
                return
            }
            
            let decoder = JSONDecoder()
            do{
                let entities = try decoder.decode([T].self, from: responseData)
                self.elements = entities
                completionHandler(self.elements,nil)
            }
            catch{
                print("Error: while trying to convert JSON to data")
                print(error)
                print(responseData)
                completionHandler(nil,error)
            }
        }
        task.resume()
    }
    
    func resetCache(){
        elements?.removeAll(keepingCapacity: false)
    }
}
