//
//  Enums.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import Foundation

enum PropertyEnum: Int{
    case clas, name, code, type, authorities, dateAdopted, number, status, sourcePublications, dateInclusion, datePublication, numberPublication,
    title, generalClassifiers, keywords
    
    func labelText() -> String {
        switch self {
        case .clas:
            return "Раздел".localized()
        case .code:
            return "Код документа".localized()
        case .number:
            return "Номер".localized()
        case .type:
            return "Вид документа".localized()
        case .authorities:
            return "Орган".localized()
        case .dateAdopted:
            return "Дата принятия".localized()
        case .status:
            return "Статус".localized()
        case .name:
            return "Наименование документа".localized()
        case .keywords:
            return "Ключевые слова".localized()
        case .sourcePublications:
            return "Источник публикации".localized()
        case .generalClassifiers:
            return "Общеправовой классификатор".localized()
        case .dateInclusion:
            return "Дата включения в Госреестр НПА".localized()
        case .datePublication:
            return "Дата опубликования".localized()
        case .numberPublication:
            return "Номер публикации".localized()
        case .title:
            return "Заголовок документа".localized()
        }
    }
}

enum FilterEnum: Int{
    case name, types, authorities, dateAdopted, number, statuses, editionText, keywords, sourcePublications, generalClassifiers, dateOfEntry, dateLimit, dateInclusion, datePublication, dateRegistration, numberRegistration, numberPublication
    
    func labelText() -> String {
        switch self {
        case .dateOfEntry:
            return "Дата вступления в силу".localized()
        case .number:
            return "Номер".localized()
        case .numberRegistration:
            return "Номер регистрации в МЮ".localized()
        case .types:
            return "Вид документа".localized()
        case .authorities:
            return "Орган".localized()
        case .dateAdopted:
            return "Дата принятия".localized()
        case .statuses:
            return "Статус".localized()
        case .name:
            return "Наименование документа".localized()
        case .editionText:
            return "Содержание".localized()
        case .keywords:
            return "Ключевые слова".localized()
        case .sourcePublications:
            return "Источники публикаций".localized()
        case .generalClassifiers:
            return "Общеправовые классификаторы".localized()
        case .dateLimit:
            return "Дата ограничения действия".localized()
        case .dateInclusion:
            return "Дата включения в Госреестр НПА".localized()
        case .datePublication:
            return "Дата опубликования".localized()
        case .dateRegistration:
            return "Дата регистрации в МЮ".localized()
        case .numberPublication:
            return "Номер публикации".localized()
        }
    }
}

enum StatusEnum: String {
    case Actual = "Действует", NotStarted = "Не вступил в силу", NotActual = "Не действует", Expired = "Утратил силу", ActualKg = "Күчүндө", NotStartedKg = "Күчүнө кире элек", NotActualKg = "Күчүн жоготкон", ExpiredKg = "Күчүн жоготту"
    
    func getColor() -> UIColor{
        switch self {
        case .Actual, .ActualKg:
            return UIColor.green
        case .NotStarted, .NotStartedKg:
            return UIColor.yellow
        case .NotActual, .NotActualKg:
            return UIColor.orange
        case .Expired, .ExpiredKg:
            return UIColor.red
        }
    }
}

enum Lang: String {
    case ru = "ru", kg = "ky"
    
    func getText() -> String {
        switch self {
        case .ru:
            return "Русский"
        case .kg:
            return "Кыргызча"
        }
    }
}
