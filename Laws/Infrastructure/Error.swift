//
//  Error.swift
//  Laws
//
//  Created by User on 11/18/17.
//  Copyright © 2017 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

enum BackendError: Error {
    case urlError
    case objectSerialization
    case noInternetConnection
    
    func getErrorText() -> String {
        switch self {
        case .noInternetConnection:
            return "Отсутсвует интернет"
        default:
            return "Произошла ошибка, попробуйте позже"
        }
    }
}
