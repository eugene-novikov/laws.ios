//
//  Status.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct Status: Codable{
    var Code: String?
    var Name: String
    var Statuses: [Status]?
    
    func transform() -> Item {
        var item = Item(code: self.Code!, name: self.Name, list: nil)
        if self.Statuses != nil{
            for classificator in self.Statuses!{
                item.appendItem(item: classificator.transform())
            }
        }
        return item
    }
}

class StatusRepository: Repository<Status>{
    init(){
        super.init(endpoint: "\(Host)/OpenData/GetClassificators.json?&Type=Status")
        
    }
}
