//
//  ButtonCell.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/14/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

protocol ButtonCellDelegate{
    func tapped(type: ButtonCellType)
}

enum ButtonCellType{
    case dropFilter,sort
}

class ButtonCell: UITableViewCell {

    private var button: UIButton!
    private var type: ButtonCellType!
    var delegate: ButtonCellDelegate?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.separatorInset = UIEdgeInsets.zero
        
        button = UIButton(frame: CGRect.zero)
        button.setTitleColor(BrandColor, for: .normal)
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 16.0
        button.layer.borderColor = BrandColor.cgColor
        button.layer.masksToBounds = true
        
        self.addSubview(button)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
        self.setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setConstraints() {
        button.autoSetDimension(.height, toSize: 30.0)
        button.autoPinEdgesToSuperviewMargins()
    }
    
    func setProperties(text: String, type: ButtonCellType){
        self.button.setTitle(text, for: .normal)
        self.type = type
    }
    
    @objc func buttonTapped(){
        delegate?.tapped(type: type)
    }

}
