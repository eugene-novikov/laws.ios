//
//  ListItemTableCell.swift
//  Laws
//
//  Created by User on 1/14/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

protocol ItemCheckDelegate{
    func checked(tag: Int)
    func unchecked(tag: Int)
}

class ListItemTableCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    var delegate: ItemCheckDelegate?
    private var checked: Bool = false
    private var checkImage: UIImage {
        get{
            return self.checked ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "unchecked")
        }}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setLabelSettings()
        self.setButtonSettings()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setLabelSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        self.label.font = titleFont
    }
    
    private func setButtonSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.caption1)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        button.titleLabel?.font = titleFont
        button.setTitle("", for: UIControlState.normal)
    }
    
    func setProperies(labelText: String, checked: Bool){
        self.checked = checked
        self.label.text = labelText
        self.button.setBackgroundImage(self.checkImage, for: UIControlState.normal)
    }
    
    private func checkToggle(){
        self.checked = !self.checked
        self.button.setBackgroundImage(self.checkImage, for: UIControlState.normal)
        if self.checked{
            delegate?.checked(tag: self.tag)
        }else{
            delegate?.unchecked(tag: self.tag)
        }
    }
    
    @IBAction func checkButton(_ sender: Any) {
        self.checkToggle()
    }
}
