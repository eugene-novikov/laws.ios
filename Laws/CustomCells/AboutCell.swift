//
//  AboutCell.swift
//  Laws
//
//  Created by User on 1/29/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class AboutCell: UITableViewCell {

    lazy private var head: UIView = {
        var head = UIView(frame: .zero)
        self.addSubview(head)
        head.autoPinEdgesToSuperviewEdges()
        return head
    }()
    private var logoView: UIImageView!
    private var label: UILabel!
    private var label2: UILabel!
    private var label3: UILabel!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setLogoSettings()
        setLabelSettings()
        setLabel2Settings()
        setLabel3Settings()
    }
    
    private func setLogoSettings(){
        logoView = UIImageView(frame: .zero)
        head.addSubview(logoView)
        logoView.autoSetDimension(.height, toSize: 100.0)
        logoView.autoSetDimension(.width, toSize: 100.0)
        logoView.image = #imageLiteral(resourceName: "logo2") //#imageLiteral(resourceName: "logo").getColored(color: BrandColor)
        logoView.autoAlignAxis(toSuperviewMarginAxis: .vertical)
        logoView.autoPinEdge(toSuperviewEdge: .top, withInset: 40.0)
    }
    
    private func setLabelSettings(){
        let labelDesciptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .headline)
        let labelFont = UIFont(descriptor: labelDesciptor, size: 0)
        label = UILabel(frame: .zero)
        head.addSubview(label)
        label.font = labelFont
        label.text = "Министерство Юстиции".localized()
        label.autoAlignAxis(toSuperviewAxis: .vertical)
        label.autoPinEdge(.top, to: .bottom, of: logoView, withOffset: 20.0)
    }
    
    private func setLabel2Settings(){
        let label2Desciptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .headline)
        let label2Font = UIFont(descriptor: label2Desciptor, size: 0)
        label2 = UILabel(frame: .zero)
        head.addSubview(label2)
        label2.font = label2Font
        label2.text = "Кыргызской Республики".localized()
        label2.autoAlignAxis(toSuperviewAxis: .vertical)
        label2.autoPinEdge(.top, to: .bottom, of: label, withOffset: 5.0)
    }
    
    private func setLabel3Settings(){
        let label3Desciptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .caption1)
        let label3Font = UIFont(descriptor: label3Desciptor, size: 0)
        label3 = UILabel(frame: .zero)
        head.addSubview(label3)
        label3.font = label3Font
        label3.text = "minjust.gov.kg"
        label3.autoAlignAxis(toSuperviewAxis: .vertical)
        label3.autoPinEdge(.top, to: .bottom, of: label2, withOffset: 5.0)
        label3.autoPinEdge(toSuperviewEdge: .bottom, withInset: 20.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
