//
//  PropertyCell.swift
//  Laws
//
//  Created by User on 1/28/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class PropertyCell: UITableViewCell {

    private var label: UILabel!
    private var value: UILabel!
    private var circle: CircleView!
    private var circleWidthConstraint: NSLayoutConstraint!
    private var circleHeightConstraint: NSLayoutConstraint!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setLabelSettings()
        setValueSettings()
        setCircleSettings()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setProperties(label: String, value: String){
        self.label.text = label
        self.value.text = value
        circleHeightConstraint.constant = 0.0
        circleWidthConstraint.constant = 0.0
    }
    
    func setProperties(label: String, value: String, color: UIColor){
        setProperties(label: label, value: value)
        circleHeightConstraint.constant = 8.0
        circleWidthConstraint.constant = 8.0
        circle.setProperties(color: color)
    }
    
    private func setLabelSettings(){
        label = UILabel(frame: .zero)
        self.addSubview(label)

        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.subheadline)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        label.font = titleFont
        label.textColor = UIColor.lightGray
        
        label.numberOfLines = 0
        label.autoSetDimension(.width, toSize: 120)
        label.autoPinEdges(toSuperviewMarginsExcludingEdge: .right)
//        label.autoPinEdge(toSuperviewEdge: .top, withInset: 16.0)
//        label.autoPinEdge(toSuperviewEdge: .left, withInset: 16.0)
//        label.autoPinEdge(toSuperviewEdge: .bottom, withInset: 16.0)
    }
    
    private func setValueSettings(){
        value = UILabel(frame: .zero)
        self.addSubview(value)
        
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.footnote)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        value.font = titleFont

        
        value.numberOfLines = 0
        value.autoPinEdge(toSuperviewEdge: .top, withInset: 16.0)
        value.autoPinEdge(toSuperviewEdge: .bottom, withInset: 16.0)
        value.autoPinEdge(.left, to: .right, of: label, withOffset: 8.0)
    }
    
    private func setCircleSettings(){
        circle = CircleView(frame: .zero)
        self.addSubview(circle)
        circle.autoPinEdge(toSuperviewMargin: .right)
//        circle.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        circle.autoAlignAxis(.horizontal, toSameAxisOf: value)
        circle.autoPinEdge(.left, to: .right, of: value, withOffset: 8.0)
        circleWidthConstraint = circle.autoSetDimension(.width, toSize: 0.0)
        circleHeightConstraint = circle.autoSetDimension(.height, toSize: 0.0)
    }
}
