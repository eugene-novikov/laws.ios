//
//  DocumentTitleTableCell.swift
//  Laws
//
//  Created by User on 1/18/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import SwipeCellKit

class DocumentTitleTableCell: SwipeTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var circle: CircleView!
    @IBOutlet weak var favoriteImageView: UIImageView!
    private var document: Document!
    
    @IBOutlet weak var favoriteImageViewWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setTitleSettings()
        self.setClassSettings()
        self.setDateSettings()
        self.setFavoriteImageSettings()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setTitleSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        self.titleLabel.font = titleFont
        self.titleLabel.textColor = UIColor.black
    }

    private func setClassSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.footnote)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        self.classLabel.font = titleFont
        self.classLabel.textColor = UIColor.lightGray
    }

    private func setDateSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.footnote)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        self.dateLabel.font = titleFont
        self.dateLabel.textColor = UIColor.lightGray
    }
    
    private func setFavoriteImageSettings(){
        favoriteImageView.image = #imageLiteral(resourceName: "starfilled").getColored(color: UIColor.lightGray)
    }
    
    func setProperties(document: Document){
        self.titleLabel.text = document.Name?.trimmingCharacters(in: CharacterSet.newlines)
        self.classLabel.text = "\(document.Type ?? "") // \(document.Class ?? "")"
        let dateString = document.DateAdopted?.toDate()?.toString() ?? ""
        self.dateLabel.text = "№\(document.Code)\r\(dateString)"
        circle.setProperties(color: (StatusEnum(rawValue: document.Status ?? "")?.getColor()) ?? UIColor.red)
        if UnitOfWork.shared().favoriteService.isFavorite(code: document.Code){
            favoriteImageViewWidthConstraint.constant = 8.0
        }else{
            favoriteImageViewWidthConstraint.constant = 0.0
        }
    }
    
}
