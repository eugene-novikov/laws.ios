//
//  TextTableCell.swift
//  Laws
//
//  Created by User on 1/12/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class TextTableCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var btConstraint: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var label: UILabel!
    var delegate: TextTableCellDelegate?
    private let buttonWidth: CGFloat = 50.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setTextSettings()
        self.setLabelSettings()
        self.setButtonSettings()
        self.setCellSettings()
        self.resetValues()
    }
    
    private func setCellSettings(){
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    private func setTextSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.body)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        textField.font = titleFont
        textField.borderStyle = UITextBorderStyle.none
        textField.delegate = self
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
    }
    
    private func setLabelSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.subheadline)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        label.font = titleFont
        label.textColor = UIColor.gray
    }
    
    private func setButtonSettings(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.caption1)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        button.titleLabel?.font = titleFont
        btConstraint.constant = 0-buttonWidth
        buttonWidthConstraint.constant = 0
    }
    
    private func resetValues(){
        self.textField.text = ""
        self.label.text = ""
        self.button.setTitle("Готово".localized(), for: UIControlState.normal)
    }

    func setProperies(labelText: String, placeHolderText: String, text: String?){
        self.resetValues()
        self.label.text = labelText
        self.textField.placeholder = placeHolderText
        if let text = text{
            self.textField.text = text
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        toggleButton(value: buttonWidth)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        toggleButton(value: 0)
        self.delegate?.endEditing(tag: self.tag, text: textField.text!)
    }
    
    func toggleButton(value: CGFloat){
        self.buttonWidthConstraint.constant = value
        self.btConstraint.constant = 0
        
        UIView.animate(withDuration: 0.3, animations: {() -> Void in
            self.layoutIfNeeded()
        })
    }
    @IBAction func buttonTap(_ sender: Any) {
        self.endEditing(true)
    }
    
}

protocol TextTableCellDelegate{
    func endEditing(tag: Int, text: String)
}
