//
//  InfoCell.swift
//  Laws
//
//  Created by User on 1/24/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {

    private var shouldSetupConstraints = true
    private var imgView: UIImageView!
    private var label: UILabel!
    private var line: UIView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.separatorInset = UIEdgeInsets.zero
        
        imgView = UIImageView(frame: CGRect.zero)
        imgView.image = #imageLiteral(resourceName: "empty")
        imgView.autoSetDimension(.height, toSize: 50.0)
        imgView.autoSetDimension(.width, toSize: 50.0)
        self.addSubview(imgView)
        
        label = UILabel(frame: CGRect.zero)
        label.numberOfLines = 0
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.subheadline)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        label.font = titleFont
        label.textColor = UIColor.gray
        self.addSubview(label)
        
        line = UIView(frame: CGRect.zero)
        line.autoSetDimension(.width, toSize: 3.0)
        line.backgroundColor = UIColor.red
        self.addSubview(line)

        self.setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setConstraints() {
            let edgesInset: CGFloat = 16.0
            imgView.autoPinEdge(toSuperviewMargin: .left)
//            imgView.autoPinEdge(toSuperviewEdge: .left, withInset: edgesInset)
            imgView.autoAlignAxis(.horizontal, toSameAxisOf: label)
            label.autoPinEdge(toSuperviewEdge: .top, withInset: edgesInset*2)
            label.autoPinEdge(toSuperviewEdge: .right, withInset: edgesInset)
            label.autoPinEdge(toSuperviewEdge: .bottom, withInset: edgesInset*2)
            label.autoPinEdge(.left, to: .right, of: imgView, withOffset: edgesInset)
            line.autoPinEdges(toSuperviewMarginsExcludingEdge: .left)
    }

    func setProperties(image: UIImage, text: String, color: UIColor){
        self.imgView.image = image
        self.label.text = text
        self.line.backgroundColor = color
    }
}
