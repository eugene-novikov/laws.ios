//
//  SourcePublication.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct SourcePublication: Codable{
    var Code: String?
    var Name: String
    var SourcePublications: [SourcePublication]?
    
    func transform() -> Item {
        var item = Item(code: self.Code!, name: self.Name, list: nil)
        if self.SourcePublications != nil{
            for classificator in self.SourcePublications!{
                item.appendItem(item: classificator.transform())
            }
        }
        return item
    }
}

class SourcePublicationRepository: Repository<SourcePublication>{
    init() {
        super.init(endpoint: "\(Host)/OpenData/GetClassificators.json?&Type=SourcePublications")
    }
}
