//
//  Authority.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct Authority: Codable{
    var Code: String?
    var Name: String
    var Authorities: [Authority]?
    
    func transform() -> Item {
        var item = Item(code: self.Code!, name: self.Name, list: nil)
        if self.Authorities != nil{
            for classificator in self.Authorities!{
                item.appendItem(item: classificator.transform())
            }
        }
        return item
    }
}

class AuthorityRepository: Repository<Authority>{
    init() {
        super.init(endpoint: "\(Host)/OpenData/GetClassificators?&Type=Authorities")
    }
}
