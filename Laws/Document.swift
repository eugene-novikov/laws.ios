//
//  Document.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct Document: Codable {
    var Code: Int
    var Class: String?
    var IsPublicInRegister: Bool?
    var IsPublicInCbd: Bool?
    var `Type`: String?
    var DateAdopted: String?
    var Number: String?
    var Status: String?
    var DateRegistration: String?
    var NumberRegistrationg: String?
    var DateOfEntry: String?
    var DateLimit: String?
    var DateInclusion: String?
    var DateAdoptedLegislativeAssembly: String?
    var DateAdoptedPeopleCongress: String?
    var DateAdoptedJk: String?
    var DatePublication: String?
    var NumberPublication: String?
    var Title: String?
    var Name: String?
    var Authorities: [Authority]?
    var SourcePublications:[SourcePublication]?
    var Keywords: [Keyword]?
    var GenerealClassifiers: [GeneralClassifier]?
    var References: [Int]?
    var Editions: [Edition]?
    
    var TitleForShare: String {
        return "\(self.Name?.trimmingCharacters(in: CharacterSet.newlines) ?? "") \nhttp://cbd.minjust.gov.kg/act/view/ru-ru/\(self.Code)"
    }
    
    func getGeneralClassifiersString(items: [GeneralClassifier], level: Int) -> String {
        var result = ""
        for item in items{
            var r = "\r"
            if items.index(where: {$0.Code == item.Code}) == 0{
                r=""
            }
            result += "\(r)\(String.init(repeating: " ", count: level))\(item.Name)"
            if let items = item.GeneralClassifiers{
                result += getGeneralClassifiersString(items: items, level: level+1)
            }
        }
        return result
    }
    
    func getKeywordsString(items: [Keyword], level: Int) -> String {
        var result = ""
        for item in items{
            var r = "\r"
            if items.index(where: {$0.Code == item.Code}) == 0{
                r=""
            }
            result += "\(r)\(String.init(repeating: " ", count: level))\(item.Name)"
            if let items = item.Keywords{
                result += getKeywordsString(items: items, level: level+1)
            }
        }
        return result
    }
    
    func getSourcePublicationsString(items: [SourcePublication], level: Int) -> String {
        var result = ""
        for item in items{
            var r = "\r"
            if items.index(where: {$0.Code == item.Code}) == 0{
                r=""
            }
            result += "\(r)\(String.init(repeating: " ", count: level))\(item.Name)"
            if let items = item.SourcePublications{
                result += getSourcePublicationsString(items: items, level: level+1)
            }
        }
        return result
    }
    
    func getAuthoritiesString(items: [Authority], level: Int) -> String {
        var result = ""
        for item in items{
            var r = "\r"
            if items.index(where: {$0.Code == item.Code}) == 0{
                r=""
            }
            result += "\(r)\(String.init(repeating: " ", count: level))\(item.Name)"
            if let items = item.Authorities{
                result += getAuthoritiesString(items: items, level: level+1)
            }
        }
        return result
    }
    
    func getValue(property: PropertyEnum) -> String {
        switch property {
        case .clas:
            return Class ?? ""
        case .name:
            return Name ?? ""
        case .code:
            return "\(Code)"
        case .type:
            return Type ?? ""
        case .authorities:
            if let _ = Authorities {
                return getAuthoritiesString(items: Authorities!, level: 0)
            }
            return ""
        case .dateAdopted:
            return DateAdopted?.toDate()?.toString() ?? ""
        case .number:
            return Number ?? ""
        case .status:
            return Status ?? ""
        case .sourcePublications:
            if let _ = SourcePublications {
                return getSourcePublicationsString(items: SourcePublications!, level: 0)
            }
            return ""
        case .dateInclusion:
            return DateInclusion?.toDate()?.toString() ?? ""
        case .datePublication:
            return DatePublication?.toDate()?.toString() ?? ""
        case .numberPublication:
            return NumberPublication ?? ""
        case .title:
            return Title ?? ""
        case .generalClassifiers:
            if let _ = self.GenerealClassifiers{
                return getGeneralClassifiersString(items: self.GenerealClassifiers!, level: 0)
            }
            return ""
        case .keywords:
            if let _ = self.Keywords {
                return getKeywordsString(items: self.Keywords!, level: 0)
            }
            return ""
        }
    }
}

class DocumentRepository: Repository<Document>{

    var url = "\(Host)/OpenData/GetDocument.json?Editions.Select=all&Replace=anchors&Code="
    
    var url2 = "\(Host)/OpenData/GetDocuments.json?Editions.Data=none&Codes="
    
    init() {
        super.init(endpoint: self.url)
    }
    
    init(codes: [String]){
        let string = {() -> String in
            var result = ""
            if let first = codes.first{
                result = first
            }
            for i in codes.filter({$0 != result}){
                result += ",\(i)"
            }
            return result
        }()
        print(string)
        super.init(endpoint: "\(self.url2)\(string)")
    }
    
    init(code: String, edition: String){
        super.init(endpoint: "\(self.url)\(code)&Editions.Data=\(edition)")
    }
}
