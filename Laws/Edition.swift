//
//  Edition.swift
//  Laws
//
//  Created by User on 1/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation

struct Edition: Codable{
    var Code: Int?
    var Name: String
    var `Type`: String
    var Images: [ImageClass]?
    var Data: String?
}

//class EditionRepository: Repository<Edition>{
//    var url = "http://cbd.minjust.gov.kg/OpenData/GetEditionHtml?DocumentCode="
//
//    init() {
//        super.init(endpoint: self.url)
//    }
//
//    init(parameters: String){
//        super.init(endpoint: "\(self.url)\(parameters)")
//    }
//}

