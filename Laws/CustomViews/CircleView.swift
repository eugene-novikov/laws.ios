//
//  CircleView.swift
//  Laws
//
//  Created by User on 1/23/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class CircleView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setProperties(color: UIColor){
        self.frame.size = CGSize(width: 8.0, height: 8.0)
        self.backgroundColor = color
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 4.0
        self.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
