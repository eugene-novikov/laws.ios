//
//  MenuCell.swift
//  Laws
//
//  Created by User on 1/29/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import Foundation
import PagingKit

class MenuCell: PagingMenuViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setProperties(){
        let titleDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: UIFontTextStyle.subheadline)
        let titleFont = UIFont(descriptor: titleDescriptor, size: 0)
        titleLabel.font = titleFont
        titleLabel.textColor = BrandColor
        titleLabel.numberOfLines = 0
    }
}
