//
//  TabBarController.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/22/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tabOne = mainStoryboard.instantiateViewController(withIdentifier: "DocumentsViewController") as! DocumentsViewController
        let tabOneBar = UITabBarItem(title: "Документы".localized(), image: #imageLiteral(resourceName: "document"), tag: 0)
        tabOne.tabBarItem = tabOneBar
        
        let tabTwo = FavoriteViewController()
        let tabTwoBar = UITabBarItem(title: "Избранные".localized(), image: #imageLiteral(resourceName: "favorite"), tag: 1)
        tabTwo.tabBarItem = tabTwoBar
        
        let tabThree = HistoryViewController()
        let tabThreeBar = UITabBarItem(title: "История".localized(), image: #imageLiteral(resourceName: "history"), tag: 2)
        tabThree.tabBarItem = tabThreeBar
        
        let tabFour = SettingsViewController()
        let tabFourBar = UITabBarItem(title: "Настройки".localized(), image: #imageLiteral(resourceName: "settings"), tag: 3)
        tabFour.tabBarItem = tabFourBar
        
        let viewControllersList = [tabOne, tabTwo, tabThree, tabFour]
        
        self.viewControllers = viewControllersList.map { UINavigationController(rootViewController: $0) }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
}
