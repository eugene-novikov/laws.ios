//
//  HistoryViewController.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/17/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class HistoryViewController: CustomViewController {
    var documentListView: DocumentListView!
    var total: Int = 0
    private var bookButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "История".localized()
        bookButton = UIBarButtonItem(image: #imageLiteral(resourceName: "book"), style: .plain, target: self, action: #selector(bookButtonAction))
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = bookButton
        
        documentListView = DocumentListView(frame: self.view.frame)
        self.view.addSubview(documentListView)
        documentListView.autoPinEdgesToSuperviewEdges()
        documentListView.delegate = self
        documentListView.swipeDelegate = self
        documentListView.navigationController = self.navigationController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !documentListView.deselectCell(){
            documentListView.setDefaults()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func bookButtonAction(sender: Any?){
        self.didBookAction()
    }
}

extension HistoryViewController: DocumentListViewDelegate{
    func didSelect(document: Document, edition: String) {
        self.openDocument(document: document, edition: edition)
    }
    
    func dropFilter() {
    }
    
    func sort() {
    }
    
    func getTableParameters() -> DocumentTableParameters {
        return DocumentTableParameters(
            emptyText: "Нет данных!".localized(),
            totalText: "Показаны последние 20 просмотренных документов".localized(),
            totalAmount: self.total,
            swipeAble: true,
            sortDesc: false,
            filter: false,
            isSortAble: false
        )
    }
    
    
    func getDocuments(page: Int, completionHandler: @escaping ([Document]?, String?) -> Void) {
        UnitOfWork.shared().historyService.getDocuments { (documents) in
            DispatchQueue.main.async{
                self.total = documents.count
                completionHandler(documents, nil)
            }
        }
    }
}

extension HistoryViewController: SwipeActionDelegate{
    func didDelete(document: Document, index: Int) {
        UnitOfWork.shared().historyService.remove(index)
    }
    
    func didFavorite(document: Document) {
        self.didFavoriteAction(code: document.Code, completionHandler: {
            
        })
    }
}
