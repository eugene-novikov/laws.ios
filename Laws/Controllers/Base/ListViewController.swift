//
//  ListViewController.swift
//  Laws
//
//  Created by User on 1/14/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

struct Item{
    var code: String
    var name: String
    private(set) var list: [Item]?
    var searchable:String {
        get{
            return code+name
        }}
    init(code: String, name: String, list: [Item]?) {
        self.code=code
        self.name=name
        if let list = list {
            self.appendList(list: list)
        }
    }
    
    mutating func appendItem(item: Item){
        if self.list == nil{
            self.list = [Item]()
        }
        self.list!.append(item)
    }
    
    mutating func appendList(list: [Item]){
        for item in list{
            self.appendItem(item: item)
        }
    }
}

protocol ListViewControllerDelegate{
    func disappear(choosed: [Item]?)
}

protocol ListViewControllerFinalDelegate{
    func final(choosed: [Item]?, tag: Int?)
}

class ListViewController: UIViewController {
    @IBOutlet weak var table: UITableView!
    private var list = [Item]()
    private var filteredList = [Item]()
    private var choosedList = [Item]()
    private var isMultipleChoise = false
    private var tag: Int?
    private var isFirstLevel = false
    var delegate: ListViewControllerDelegate?
    var finalDelegate: ListViewControllerFinalDelegate?
    
    var resultSearchController: UISearchController = UISearchController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.tableFooterView = UIView(frame: CGRect.zero)
        let nib = UINib(nibName: "ListItemTableCell", bundle: nil)
        self.table.register(nib, forCellReuseIdentifier: "itemCell")
        self.table.dataSource = self
        self.table.delegate = self
        
        self.table.rowHeight = UITableViewAutomaticDimension
        self.table.estimatedRowHeight = 140
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            
            self.table.tableHeaderView = controller.searchBar
            return controller
        })()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isFirstLevel {
            self.finalDelegate?.final(choosed: self.choosedList, tag: self.tag)
        }
        self.delegate?.disappear(choosed: self.choosedList)
        if self.resultSearchController.isActive{
            self.resultSearchController.dismiss(animated: true, completion: nil)
        }
    }
    
    func setProperties(items: [Item], choosed: [Item]?, isMultipleChoise: Bool, tag: Int?, isFirstLevel: Bool) {
        self.isFirstLevel = isFirstLevel
        self.isMultipleChoise = isMultipleChoise
        for item in items {
            self.list.append(item)
        }
        if let choosed = choosed{
            for item in choosed{
                self.choosedList.append(item)
            }
        }
        if let tag = tag{
            self.tag = tag
        }
        self.table?.reloadData()
    }
    
    @IBAction func Close(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultSearchController.isActive{
            return filteredList.count
        }else{
            return list.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "itemCell") as! ListItemTableCell
        let item = resultSearchController.isActive ? filteredList[indexPath.row] : list[indexPath.row]
        var checked = false
        if let _ = self.choosedList.first(where: {$0.code == item.code}){
            checked = true
        }
        cell.setProperies(labelText: item.name, checked: checked)
        cell.tag = indexPath.row
        cell.delegate = self
        if let _ = item.list {
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCellSelectionStyle.gray
        } else{
            cell.accessoryType = UITableViewCellAccessoryType.none
            cell.selectionStyle = UITableViewCellSelectionStyle.none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.resultSearchController.isActive ? filteredList[indexPath.row] : list[indexPath.row]
        if let items = item.list{
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
            self.navigationController?.pushViewController(vc, animated: true)
            vc.setProperties(items: items, choosed: self.choosedList, isMultipleChoise: self.isMultipleChoise, tag: self.tag, isFirstLevel: false)
            vc.title = item.name
            vc.delegate = self
        }
        self.table.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ListViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        self.filteredList.removeAll(keepingCapacity: false)
        self.filteredList = self.list.filter({(item: Item) -> Bool in
            let stringMatch = item.searchable.lowercased().contains(searchController.searchBar.text!.lowercased())
            if searchController.searchBar.text!.isEmpty{
                return true
            }
            return stringMatch
        })
        self.table.reloadData()
    }
}

extension ListViewController: ItemCheckDelegate{
    func checked(tag: Int) {
        setCheckValue(index: tag, checked: true)
    }
    
    func unchecked(tag: Int) {
        setCheckValue(index: tag, checked: false)
    }

    private func setCheckValue(index: Int, checked: Bool){
        let item: Item = self.resultSearchController.isActive ? filteredList[index] : list[index]
        
        if checked {
            if !isMultipleChoise{
                if let prev = self.choosedList.first {
                    self.choosedList.removeAll()
                    if let prevIndex = (self.resultSearchController.isActive ? filteredList : list).index(where: {$0.code == prev.code}) {
                        self.table.reloadRows(at: [IndexPath(row: prevIndex, section: 0)], with: UITableViewRowAnimation.none)
                    }
                }
            }
            self.choosedList.append(item)
        }else{
            if let i = self.choosedList.index(where: {$0.code == item.code}){
                self.choosedList.remove(at: i)
            }
        }
    }
}

extension ListViewController: ListViewControllerDelegate{
    func disappear(choosed: [Item]?) {
        self.choosedList.removeAll()
        if let choosed = choosed {
            for item in choosed {
                self.choosedList.append(item)
            }
        }
        if let tag = tag {
            self.tag = tag
        }
        self.table.reloadData()
    }
    
    
}
