//
//  PagingViewController.swift
//  Laws
//
//  Created by Anvar Nurmatov on 5/18/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit
import PagingKit

class PagingViewController: CustomViewController {
    private var menuViewController: PagingMenuViewController!
    private var contentViewController: PagingContentViewController!
    var dataSource: [(menuTitle: String, vc: UIViewController)]?
    var barButtons: [UIBarButtonItem]?
    var widthForMenuBar: CGFloat = 230.0
    var reloadDataWhenDidAppear: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItems = self.barButtons
        self.menuViewController.register(nib: UINib(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
        self.menuViewController.registerFocusView(nib: UINib(nibName: "PagingFocusView", bundle: nil))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.dataSource != nil, self.reloadDataWhenDidAppear {
            self.reloadData()
            self.reloadDataWhenDidAppear = false
        }
    }
    
    func currentIndex() -> Int{
        return contentViewController.currentPageIndex
    }
    
    func reloadData(){
        self.menuViewController.reloadData()
        self.contentViewController.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController.dataSource = self
            menuViewController.delegate = self
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController.dataSource = self
            contentViewController.delegate = self
        }
    }
    
    func viewPageAt(index: Int){
        
    }
}

extension PagingViewController: PagingMenuViewControllerDataSource{
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        if let dataSource = self.dataSource{
            return dataSource.count
        }
        return 0
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return widthForMenuBar
    }
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: index) as! MenuCell
        cell.setProperties()
        if let dataSource = self.dataSource {
            cell.titleLabel.text = dataSource[index].menuTitle
        }
        return cell
    }
}

extension PagingViewController: PagingContentViewControllerDataSource{
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        if let dataSource = self.dataSource {
            return dataSource.count
        }
        return 0
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        if let dataSource = self.dataSource {
            return dataSource[index].vc
        }
        return UIViewController()
    }
    
    func contentViewController(viewController: PagingContentViewController, didFinishPagingAt index: Int, animated: Bool) {
        if let dataSource = self.dataSource, dataSource.count>0{
            viewPageAt(index: index)
        }
    }
}

extension PagingViewController: PagingMenuViewControllerDelegate{
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController.scroll(to: page, animated: true)
    }
}

extension PagingViewController: PagingContentViewControllerDelegate{
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController.scroll(index: index, percent: percent, animated: true)
    }
}
