//
//  FavoriteViewController.swift
//  Laws
//
//  Created by User on 1/24/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class FavoriteViewController: CustomViewController {
    
    var documentListView: DocumentListView!
    var total: Int = 0
    private var bookButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Избранные".localized()
        bookButton = UIBarButtonItem(image: #imageLiteral(resourceName: "book"), style: .plain, target: self, action: #selector(bookButtonAction))
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = bookButton
        
        documentListView = DocumentListView(frame: self.view.frame)
        self.view.addSubview(documentListView)
        documentListView.autoPinEdgesToSuperviewEdges()
        documentListView.delegate = self
        documentListView.swipeDelegate = self
        documentListView.navigationController = self.navigationController
        
//        documentListView.setDefaults()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !documentListView.deselectCell(){
            documentListView.setDefaults()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func bookButtonAction(sender: Any?){
        self.didBookAction()
    }
}

extension FavoriteViewController: DocumentListViewDelegate{
    func didSelect(document: Document, edition: String) {
        self.openDocument(document: document, edition: edition)
    }
    
    func dropFilter() {
        
    }
    
    func sort() {
        
    }
    
    func getTableParameters() -> DocumentTableParameters {
        return DocumentTableParameters(
            emptyText: "Избранные документы отсутствуют".localized(),
            totalText: "Всего избранных документов".localized() + ": \(self.total)",
            totalAmount: self.total,
            swipeAble: false,
            sortDesc: false,
            filter: false,
            isSortAble: false
        )
    }

    
    func getDocuments(page: Int, completionHandler: @escaping ([Document]?, String?) -> Void) {
        UnitOfWork.shared().favoriteService.getDocuments { (documents) in
            DispatchQueue.main.async{
                self.total = documents.count
                completionHandler(documents, nil)
            }
        }
    }
}

extension FavoriteViewController: SwipeActionDelegate{
    func didDelete(document: Document, index: Int) {
        
    }
    
    func didFavorite(document: Document) {
        self.didFavoriteAction(code: document.Code, completionHandler: {
        })
    }
}


