//
//  FilterViewController.swift
//  Laws
//
//  Created by User on 1/12/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

protocol FilterViewControllerDelegate{
    func filterApply(properties: [FilterEnum: Any?])
}

class FilterViewController: CustomViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    private var properties: [FilterEnum: Any?] = [:]
    var delegate: FilterViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table.delegate = self
        self.table.dataSource = self
        self.table.tableFooterView = UIView(frame: CGRect.zero)

        let nib = UINib(nibName: "TextTableCell", bundle: nil)
        self.table.register(nib, forCellReuseIdentifier: "textCell")
        let nib1 = UINib(nibName: "DateTableCell", bundle: nil)
        self.table.register(nib1, forCellReuseIdentifier: "dateCell")

        
        self.table.estimatedRowHeight = 85.0
        self.table.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setProperties(properties: [FilterEnum: Any?]?){
        if let properties = properties{
            self.properties = properties
        }
    }
    
    @IBAction func apply(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate?.filterApply(properties: self.properties)
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 17
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let filter = FilterEnum(rawValue: indexPath.row) else {
            let cell = table.dequeueReusableCell(withIdentifier: "cell")
            cell!.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell!.textLabel?.text = "Раздел".localized()
            return cell!
        }
        
        switch filter {
        case .authorities, .types, .statuses, .keywords, .generalClassifiers, .sourcePublications:
            var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
            }
            cell!.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell!.textLabel?.text = filter.labelText()
            cell!.detailTextLabel?.text = (self.properties[filter] as? [Item])?.toString()
            return cell!
            
        case .number, .numberRegistration, .name, .editionText, .numberPublication:
            let cell = table.dequeueReusableCell(withIdentifier: "textCell", for: indexPath) as! TextTableCell
            cell.delegate = self
            cell.setProperies(labelText: filter.labelText(), placeHolderText: "Введите текст".localized(), text: properties[filter] as? String)
            cell.tag = filter.rawValue
            return cell
            
    case .dateOfEntry, .dateAdopted, .dateLimit, .dateInclusion, .datePublication, .dateRegistration:
            let dateFrom = (properties[filter] as? [Date?])?[0]
            let dateTo = (properties[filter] as? [Date?])?[1]
            let cell = table.dequeueReusableCell(withIdentifier: "dateCell", for: indexPath) as! DateTableCell
            cell.delegate = self
            cell.setProperies(labelText: filter.labelText(), dateFrom: dateFrom, dateTo: dateTo)
            cell.tag = filter.rawValue
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let filter = FilterEnum(rawValue: indexPath.row){
            switch filter {
            case .authorities, .types, .statuses, .keywords, .generalClassifiers, .sourcePublications:
                performSegue(withIdentifier: "ShowList", sender: indexPath)
                self.table.deselectRow(at: indexPath, animated: true)
            default:
                return
            }
        }
    }

    @IBAction func Close(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    func callListViewController(filter: FilterEnum, items: [Item], isMultipleChoice: Bool, segue: UIStoryboardSegue){
        DispatchQueue.main.async{
            if let listController = segue.destination as? ListViewController {
                listController.finalDelegate = self
                
                let filterValue = self.properties[filter] as? [Item]
                listController.setProperties(items: items, choosed: filterValue, isMultipleChoise: isMultipleChoice, tag: filter.rawValue, isFirstLevel: true)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="ShowList"{
            if let indexPath = self.table.indexPathForSelectedRow, let filter = FilterEnum(rawValue: indexPath.row) {
                let vc = segue.destination as? ListViewController
                vc?.title = filter.labelText()
                switch filter {
                case .types:
                    UnitOfWork.shared().documentTypeRep.getAll(completionHandler: { (data, _) in
                        var items = [Item]()
                        if let data = data {
                            for element in data {
                                items.append(element.transform())
                            }
                        }
                        self.callListViewController(filter: filter, items: items, isMultipleChoice: true, segue: segue)
                    })
                    
                case .authorities:
                    UnitOfWork.shared().authorityRep.getAll(completionHandler: { (data, _) in
                        var items = [Item]()
                        if let data = data {
                            for element in data {
                                items.append(element.transform())
                            }
                        }
                        self.callListViewController(filter: filter, items: items, isMultipleChoice: true, segue: segue)
                    })
                case .statuses:
                    UnitOfWork.shared().statusRepository.getAll(completionHandler: { (data, _) in
                        var items = [Item]()
                        if let data = data {
                            for element in data {
                                items.append(element.transform())
                            }
                        }
                        self.callListViewController(filter: filter, items: items, isMultipleChoice: true, segue: segue)
                    })
                case .keywords:
                    UnitOfWork.shared().keywordRep.getAll(completionHandler: { (data, _) in
                        var items = [Item]()
                        if let data = data {
                            for element in data {
                                items.append(element.transform())
                            }
                        }
                        self.callListViewController(filter: filter, items: items, isMultipleChoice: true, segue: segue)
                    })
                case .generalClassifiers:
                    UnitOfWork.shared().generalClassifierRep.getAll(completionHandler: { (data, _) in
                        var items = [Item]()
                        if let data = data {
                            for element in data {
                                items.append(element.transform())
                            }
                        }
                        self.callListViewController(filter: filter, items: items, isMultipleChoice: true, segue: segue)
                    })
                case .sourcePublications:
                    UnitOfWork.shared().sourcePublicationRep.getAll(completionHandler: { (data, _) in
                        var items = [Item]()
                        if let data = data {
                            for element in data {
                                items.append(element.transform())
                            }
                        }
                        self.callListViewController(filter: filter, items: items, isMultipleChoice: true, segue: segue)
                    })
                default:
                    break
                    
                }
            }
        }
    }
}

extension FilterViewController{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bottomConstraint.constant = 0
        
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo
        let keyboardFrame = info?[UIKeyboardFrameEndUserInfoKey] as! CGRect
        let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.height, 0.0)
        table.contentInset = contentInset
        table.scrollIndicatorInsets = contentInset
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInset = UIEdgeInsets.zero
        table.contentInset = contentInset
        table.scrollIndicatorInsets = contentInset
    }
}

extension FilterViewController: ListViewControllerFinalDelegate {
    
    func final(choosed: [Item]?, tag: Int?) {
        if let choosed = choosed, let tag = tag{
            if let filter = FilterEnum(rawValue: tag){
                self.properties[filter] = choosed
                self.table.reloadRows(at: [IndexPath(row: filter.rawValue, section: 0)], with: UITableViewRowAnimation.automatic)
            }
        }
    }
}

extension FilterViewController: DateTableCellDelegate{
    func resetDate(tag: Int){
        if let filter = FilterEnum(rawValue: tag){
            self.properties.removeValue(forKey: filter)
        }
    }
    
    func setFrom(tag: Int, date: Date) {
        if let filter = FilterEnum(rawValue: tag){
            var dates = self.properties[filter] as? [Date?]
            if (dates == nil || dates?.count==0){
                dates = [Date?]()
                dates!.append(date)
                dates!.append(nil)
            }else{
                dates![0] = date
            }
            self.properties[filter] = dates
        }
    }
    
    func setTo(tag: Int, date: Date) {
        if let filter = FilterEnum(rawValue: tag){
            var dates = self.properties[filter] as? [Date?]
            if (dates == nil || dates?.count==0){
                dates = [Date?]()
                dates!.append(nil)
                dates!.append(date)
            }else{
                if (dates!.count==1){
                    dates!.append(date)
                }else{
                    dates![1] = date
                }
            }
            self.properties[filter] = dates
        }
    }
}

extension FilterViewController: TextTableCellDelegate{
    func endEditing(tag: Int, text: String) {
        if let filter = FilterEnum(rawValue: tag){
            self.properties[filter] = text
        }
    }
}
