//
//  DocumentEditionsViewController.swift
//  Laws
//
//  Created by User on 1/29/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class DocumentEditionsViewController: CustomViewController {

    lazy var table: UITableView = {
        var table = UITableView(frame: .zero)
        self.view.addSubview(table)
        table.tableFooterView = UIView(frame: .zero)
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        table.autoPinEdgesToSuperviewEdges()
        
        self.title = "Редакции".localized()
        return table
    }()
    private var updatedConstraint:Bool = false
    private var document: Document!
    private var currentionEdition: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setProperties(document: Document, edition: String){
        self.document = document
        self.currentionEdition = edition
    }
    
}

extension DocumentEditionsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return document.Editions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
        if let edition = document.Editions?[indexPath.row]{
            cell.textLabel?.text = edition.Name
            cell.detailTextLabel?.text = "\(edition.Code ?? 0)"
            if edition.Code == Int(currentionEdition) ?? document.Editions?.first?.Code {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }else {
                cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let edition = document.Editions?[indexPath.row]{
            self.openDocument(document: document, edition: "\(edition.Code!)")
        }
        table.deselectRow(at: indexPath, animated: true)
    }
}

