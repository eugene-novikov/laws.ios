//
//  DocumentPropertyViewController.swift
//  Laws
//
//  Created by User on 1/25/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

class DocumentPropertyViewController: CustomViewController {

    lazy var table: UITableView = {
        var table = UITableView(frame: .zero)
        self.view.addSubview(table)
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.register(PropertyCell.self, forCellReuseIdentifier: "propertyCell")
        table.estimatedRowHeight = 85.0
        table.rowHeight = UITableViewAutomaticDimension

        table.autoPinEdgesToSuperviewEdges()
        
        self.title = "Реквизиты".localized()
        return table
    }()
    private var updatedConstraint:Bool = false
    private var document: Document!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setProperties(document: Document){
        self.document = document
    }
    
}

extension DocumentPropertyViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let property = PropertyEnum(rawValue: indexPath.row){
            if document.getValue(property: property).isEmpty{
                return 0.0
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let property = PropertyEnum(rawValue: indexPath.row){
            let cell = table.dequeueReusableCell(withIdentifier: "propertyCell") as! PropertyCell
            if property == PropertyEnum.status{
                cell.setProperties(label: property.labelText(), value: document.getValue(property: property), color: StatusEnum(rawValue: document.getValue(property: property))?.getColor() ?? UIColor.red)
            }else{
                cell.setProperties(label: property.labelText(), value: document.getValue(property: property))
            }
            return cell
        }
        return UITableViewCell()
    }
    
}
