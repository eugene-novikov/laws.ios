//
//  DocumentReferencesViewController.swift
//  Laws
//
//  Created by User on 1/27/18.
//  Copyright © 2018 MinJustKyrgyzstan. All rights reserved.
//

import UIKit

final class DocumentReferencesViewController: CustomViewController {
    
    var documentListView: DocumentListView!
    var total: Int = 0
    private var document: Document!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Ссылки".localized()
        documentListView = DocumentListView(frame: self.view.frame)
        self.view.addSubview(documentListView)
        documentListView.autoPinEdgesToSuperviewEdges()
        documentListView.delegate = self
        documentListView.swipeDelegate = self
        documentListView.navigationController = self.navigationController
        documentListView.setDefaults()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !documentListView.deselectCell(){
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setProperties(document: Document){
        self.document = document
    }
}

extension DocumentReferencesViewController: DocumentListViewDelegate{
    func didSelect(document: Document, edition: String) {
        self.openDocument(document: document, edition: edition)
    }
    
    func dropFilter() {}
    
    func sort() {}
    
    func getTableParameters() -> DocumentTableParameters {
        return DocumentTableParameters(
            emptyText: "Ссылающиеся документы отсутствуют".localized(),
            totalText: "Всего ссылок".localized() + ": \(self.total)\r[\((self.document.Title ?? self.document.Name)!)]",
            totalAmount: self.total,
            swipeAble: false,
            sortDesc: false,
            filter: false,
            isSortAble: false
        )
    }
    
    func getDocuments(page: Int, completionHandler: @escaping ([Document]?, String?) -> Void) {
        if let references = document.References, references.count>0{
                DocumentRepository(codes: references.map({String($0)})).getAll(completionHandler: { (documents, error) in
                    if let _ = error {
                        completionHandler(nil, "Ошибка".localized())
                    }
                    self.total = documents?.count ?? 0
                    completionHandler(documents, nil)
                })
        }else{
            completionHandler(nil, "Ссылающиеся документы отсутствуют".localized())
        }
    }
}

extension DocumentReferencesViewController: SwipeActionDelegate{
    func didDelete(document: Document, index: Int) {
        
    }
    
    func didFavorite(document: Document) {
        self.didFavoriteAction(code: document.Code, completionHandler: {
            
        })
    }
}


